/*
 * Bubble sort an array of randomly generated values.
 * https://www.geeksforgeeks.org/bubble-sort/
 */
#include <stdio.h>
#include <stdlib.h>

#define USAGE "usage: %s <elements>\n"

int *array;

/* XOR swap two values in place */
void
swap(int *a, int *b)
{
	*a = *a ^ *b;
	*b = *a ^ *b;
	*a = *a ^ *b;
}

/* Create array */
void
createArray(int count)
{
	int i;
	array = (int *)malloc(count * sizeof(int));
	for(i=0; i<count; i++)
		array[i] = rand() & 0xff;
}

/* Sort array */
void
sortArray(int count)
{
	int i, j;
	for (i=0; i<count; i++)
		for (j=count-1; j>i; j--)
			if ( array[j-1]>array[j] )
				swap(&(array[j-1]),&(array[j]));
}

/* Print Array */
void
printArray(char *prefix, int count)
{
	int i;
	printf("%s ", prefix);
	for(i=0; i<count; i++)
		printf("%3d ", array[i]);
	printf("\n");
}

int
main(int argc, char **argv)
{
	int		elements = 0;

	/* Error check command line. */
	if ( argc != 2 )
	{
		printf(USAGE, argv[0]);
		exit(1);
	}

	/* Get number of elements. */
	elements = atoi(argv[1]);
	if (elements < 1)
	{
		printf("Invalid number of elements: %d\n", elements);
		printf(USAGE, argv[0]);
		exit(1);
	}

	/* Create the array */
	createArray(elements);
	printArray("Before:", elements);

	/* Sort the array */
	sortArray(elements);
	printArray("After :", elements);

	/* B'Bye*/
	exit(0);
}
