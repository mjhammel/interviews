// Program to convert a string to uppercase.
// Note: characters can be treated as numbers in the ASCII table.
// lower to upper: subtract 32.
// upper to lower: add 32.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main()
{
	char *val;
	int i;

	val = (char *)malloc(4);
	memset(val, 0, 4);
	sprintf(val, "oNe");
	printf("val = %s\n", val);
	for(i=0; i<strlen(val); i++)
		if ( (val[i] > 96) && (val[i]<122) )
			val[i] -= 32;
	printf("val = %s\n", val);
	for(i=0; i<strlen(val); i++)
		if ( (val[i] > 64) && (val[i]<91) )
			val[i] += 32;
	printf("val = %s\n", val);
	return (0);
}
