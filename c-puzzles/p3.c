#include <stdio.h>

// Only runs once because the continue causes the while() test to be evaluated.
// Do-while only guarantees one run of the loop before the first test..
enum {false,true};

int main()
{
        int i=1;
        do
        {
            printf("%d\n",i);
            i++;
            if(i < 15)
                continue;
        }while(false);
        return 0;
}
