#include<stdio.h>

// This won't work because of signed/unsigned comparison later.
#define TOTAL_ELEMENTS (sizeof(array) / sizeof(array[0]))
int array[] = {23,34,12,17,204,99,16};

int main()
{
    int d;

    printf("TOTAL elements: (should be 7) %d\n", TOTAL_ELEMENTS);
    for(d=-1;d <= (TOTAL_ELEMENTS-2);d++)
    	printf("%d\n",array[d+1]);

    return 0;
}
