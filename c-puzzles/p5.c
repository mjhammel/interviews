// This fails because the "default" case is misspelled, but the compiler doesn't flag it.
// In fact, you could use any value there and the compiler thinks its fine.
#include<stdio.h>
int main()
{
    int a=10;
    switch(a)
    {
        case 1:
            printf("ONE\n");
            break;
        case 2:
            printf("TWO\n");
            break;
        defalut:
            printf("NONE\n");
    }
    return 0;
}
