#include <stdio.h>
#include <unistd.h>

// This doesn't print hello-out until exit because stderr gets flushed immediately
// but fprintf must buffer stdout waiting for the newline.
// On exit, the stdout buffer is flushed.
int main()
{
    int cnt = 0;
    while(1)
    {
        fprintf(stdout,"hello-out");
        fprintf(stderr,"hello-err");
        sleep(1);
        if (cnt++ == 3 )
            break;
    }
    // fprintf(stdout,"\n");
    return 0;
}
