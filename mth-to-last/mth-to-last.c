/* 
 * Given an singly-linked list, find the mth-to-last entry.
 * Goal is to move ahead m-entries, set a marker and then
 * have the marker follow till the last entry is found.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_NODES   10

typedef struct _node_t {
    struct _node_t *next;
    int             idx;
} NODE_T;

NODE_T *get_mth_to_last( NODE_T *head, int m )
{
    NODE_T  *lead = head;
    NODE_T  *follow = NULL;
    int     i;

    if ( head == NULL )
        return NULL;

    for (i=0; i<m; i++)
    {
        if ( lead->next )
            lead = lead->next;
        else
            return NULL;
    }

    follow = head;
    while (lead != NULL)
    {
        lead = lead->next;
        follow = follow->next;
    }
    return(follow);
}

NODE_T *init_list()
{
    NODE_T *node = NULL, *last=NULL, *first=NULL;
    int i;

    for(i=0; i<MAX_NODES; i++)
    {
        node = (NODE_T *)calloc(1, sizeof(NODE_T));
        node->idx = rand() % 256;
        if ( first == NULL )
            first = node;
        if ( last != NULL )
            last->next = node;
        last = node;
    }
    return first;
}

void print_node(NODE_T *node)
{
    if (node)
        printf("%3d\n", node->idx);
    else
        printf("No node!\n");
}

void print_list(NODE_T *head)
{
    NODE_T *node = head;
    while (node)
    {
        printf("%3d ", node->idx);
        node = node->next;
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    NODE_T  *head = NULL, *node = NULL;
    int     idx;

    /* No error checking here - just die. */
    idx = atoi(argv[1]);

    /* Initialize RNG */
    srand(time(NULL));

    head = init_list();
    print_list(head);
    node = get_mth_to_last(head, idx);
    if ( node )
        print_node(node);
    else
        printf("No such node.\n");

}
