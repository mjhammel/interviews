/*
 * Start two threads and let them run indepently based on a global variable.
 * The global is not thread safe because it is not wrapped by a mutex.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int globalState = 0;

/* First thread */
void *
thread1()
{
	while (!globalState)
	{
		printf("In Thread 1\n");
		sleep(1);
	}
	pthread_exit(0);
}

/* Second thread */
void *
thread2()
{
	while (!globalState);
	while (globalState)
	{
		printf("In Thread 2\n");
		sleep(1);
	}
	pthread_exit(0);
}

int
main(int argc, char **argv)
{
	int			rc;
	pthread_t 	threads[2];

	/* Create first thread. */
	rc = pthread_create(&threads[0], NULL, thread1, NULL);
	if ( rc )
	{
		printf("1. pthread_create returned: %d\n", rc);
		exit(1);
	}

	/* Create second thread. */
	rc = pthread_create(&threads[0], NULL, thread2, NULL);
	if ( rc )
	{
		printf("2. pthread_create returned: %d\n", rc);
		exit(1);
	}

	/* Give them a reason to exit. */
	sleep(3);
	globalState = ~globalState;
	sleep(3);
	globalState = ~globalState;

	/* B'Bye. */
	exit(0);
}
