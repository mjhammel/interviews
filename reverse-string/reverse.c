/*
 * Reverse a string in place using xor
 * Source: http://stackoverflow.com/questions/198199/how-do-you-reverse-a-string-in-place-in-c-or-c
 */

#include <stdio.h>
#include <string.h>

int main (int argc, char **argv)
{
    char    *head = argv[1];
    char    *end  = head;

    printf("Initial string: %s\n", head);
    while (end && *end)
        end++;

    for(--end; head<end; head++, end--)
    {
        *head = *head ^ *end;
        *end  = *end  ^ *head;
        *head = *head ^ *end;
    }

    printf("Reverse string: %s\n", argv[1]);
}
