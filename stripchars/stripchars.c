/*
 * Strip a substring from a source string.
 * Assume source and substring only contain
 * letters 'a' to 'z'.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
 * Needle already converted to lowercase.
 * 'a' is first lowercase == ASCII 97,
 * so we just subtract the 97 from the ASCII value
 * to get an index into an array of 26 characters.
 */
char *hashNeedle(char *needle)
{
    char *hash;
    char *ptr;

    hash = (char *)calloc(1,26);
    for(ptr=needle; *ptr; ptr++)
    {
        hash[*ptr-97] = 1;
    }
    return(hash);
}

void stripChars(char *src, char *needle)
{
    char *hash;
    char *ptr, *current;

    /*
     * Convert both haystack and needle
     * to lowercase. Then generate the hash
     * for the needle.
     */
    for(ptr=src; *ptr; ++ptr)
    {
        *ptr = tolower(*ptr);
    }
    for(ptr=needle; *ptr; ++ptr)
    {
        *ptr = tolower(*ptr);
    }
    hash = hashNeedle(needle);

    /* Save a post-needle pointer. */
    current=src;

    /* Iterate over the string. */
    for(ptr=src; *ptr!='\0'; ptr++)
    {
        /* Skip over the needle */
        if (!hash[*ptr-97])
        {
            /* Copy back characters after needle */
            if (ptr != current )
                *current = *ptr;
            current++;
        }
    }
    *current = '\0';

    free(hash);
}

int main(int argc, char **argv)
{
    char    src[64];
    char    needle[64];

    sprintf(src,"Michael");
    sprintf(needle,"ce");
    fprintf(stderr, "src: %s, needle: %s, ", src, needle);
    stripChars(src, needle);
    fprintf(stderr, "result: %s\n", src);

    sprintf(src,"Michael");
    sprintf(needle,"xw");
    fprintf(stderr, "src: %s, needle: %s, ", src, needle);
    stripChars(src, needle);
    fprintf(stderr, "result: %s\n", src);

    sprintf(src,"Brinda");
    sprintf(needle,"nw");
    fprintf(stderr, "src: %s, needle: %s, ", src, needle);
    stripChars(src, needle);
    fprintf(stderr, "result: %s\n", src);
}
