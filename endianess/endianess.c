/* 
 * Use a union to set an int value and then check if a char bit is set.
 * If it is then we're on a Little Endian system, otherwise we're on
 * a Big Endian system.
 */
#include <stdio.h>

int main (int argc, char **argv)
{
    union {
        int     num;
        char    field;
    } val;
    val.num = 1;
    printf("%s endian system\n", (val.field)?"Little":"Big");
}
