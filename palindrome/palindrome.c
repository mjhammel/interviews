/*
 * Test of a string is a palindrome.
 * Algorithm:
 *  Get pointers for start and end of string.
 *  assume it is a palindrome
 *  while start_pointer < end_pointer
 *      if start_pointer != end_pointer
 *          fail test and exit
 *      increment start_pointer
 *      decrement end_pointer
 *  Test passes
 */

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char **argv)
{
    char    *head  = argv[1];
    char    *startp= head;
    char    *endp  = head;

    printf("Initial string: %s\n", head);

    /*
     * Get end of string without strlen(), 
     * but stop at last character,
     * not terminating character!
     */
    while (endp && *endp)
        endp++;
    endp--;

    /* Iterate from both ends and test. */
    for(; startp<endp; startp++, endp--)
    {
        printf("s: %c / e: %c\n", *startp, *endp);
        if ( *startp != *endp )
        {
            printf("Not a palindrome.\n");
            exit(1);
        }
    }
    printf("Is a palindrome.\n");
}
