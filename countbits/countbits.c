/*
 * Given an integer on the command line, count the number
 * of bits set to 1 in the value.
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * Shift and count
 */
int
countbits(int p)
{
	int c = 0;
	while (p)
		c += (p & 0x01),
		p>>=1;
	return c;
}

int
main(int argc, char **argv)
{
	printf("0x%x\n", atoi(argv[argc-1]));
	printf("Bits on: %d\n", countbits(atoi(argv[argc-1])));
	return(0);
}
