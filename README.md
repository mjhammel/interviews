These are a few example problems from various interviews I've been on.
There are also a few examples of problems I researched as practice before going on interviews.
Sometimes, you just gotta brush up a bit on stuff you never use....

The example code is very short and mostly documented.  Just read it to figure things out.
You can compile just about everything (except for code that was written to specifically show 
compilation problems). After compiling you can run the executable.  Some prompt for input.
Others just run.  Some give meaningful output.  Others do not.  Again, just read the code.

binarytree                      Implement a binary tree (btree)
btree-traversal                 Post-order, In-order and Pre-order traversal of btree
bubblesort                      Implements a bubble sort.
c-puzzles                       Examples of code that may or may not compile, and why.
countbits                       Count the number bits that are set in an integer value.
endianess                       Determine if the host is big- or little-endian.
fizzbuzz                        Prints nums from 1..100 where mod(3) prints Fizz, mod(5) print Buzz and both print FizzBuzz
heap                            Implements a heap structure
heapify                         Converting a binary tree into a Heap (Max Heap)
insertsort                      Implement an insert sort
linklist                        Creates, inserts and deletes in singly-linked list.
list-reverse                    Reverses the order of a single linked list.
lkm                             Implements a simple Linux kernel module.
obfuscated                      Bound a ball around the terminal.
palindrome                      Determine if a string is a palindrome.
reverse-string                  Reverse a string in place using xor.
scheduler_and_array             Schedule functions at specified time offsets.
selectsort                      Implements a select sort.
shuffle                         Shuffle a deck of cards.
simple-btree                    Create btree using: 1) 3 pointers, 2) one pointer, 3) insert method
stringreverse                   Same as reverse-string, but slightly different implementation.
stripchars                      Remove a string of characters from another string.
threads                         Create two threads using pthreads.
unions                          Example of using unions in a data structure.
