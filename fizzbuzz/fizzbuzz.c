/*
 * FizzBuzz implementation.
 * For n=1..100
 *      if n%3=0 print Fizz
 *      if n%5=0 print Buzz
 *      if both  print FizzBuzz
 *      else print num
 * Source: http://wiki.c2.com/?FizzBuzzTest
 */

#include <stdio.h>

#define FIZZ    "fizz"
#define BUZZ    "buzz"
int main (int argc, char **argv)
{
    char    fizz, buzz;
    char    i;

#if DOA
    /*
     * If i%3 && i%5 : fizzbuzz
     * If i%3 only   : fizz
     * If i%5 only   : buzz
     * otherwise print number
     */
    for (i=1; i<=100; i++)
    {
        fizz=i%3;
        buzz=i%5;

        if ( !fizz & !buzz ) printf("%s%s\n", FIZZ, BUZZ);
        else if ( !fizz )    printf("%s\n", FIZZ);
        else if ( !buzz )    printf("%s\n", BUZZ);
        else                 printf("%d\n", i);
    }
#else
    for (i=1;i<=100;i++)
    {
        fizz = i%3;
        buzz = i%5;
        if ( fizz && buzz )
            printf("%d", i);
        else {
            if ( !fizz )
                printf("fizz");
            if ( !buzz )
                printf("buzz");
        }
        printf("\n");
    }
#endif

}
