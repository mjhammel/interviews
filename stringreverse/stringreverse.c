/*
 * Use XOR Swap Algorithm to swap bytes in place.
 * http://en.wikipedia.org/wiki/XOR_swap_algorithm
 */

#include <stdio.h>
#include <stdlib.h>

void
strrev(char *p)
{
	char *q = p;

	/* Find end of string */
	while ( *q ) ++q;

	/* Walk string, reversing opposite ends in place. */
	for(--q; p<q; ++p, --q) {
		*p = *p ^ *q,
		*q = *p ^ *q,
		*p = *p ^ *q;
	}
}

int
main(int argc, char **argv)
{
	if ( argc == 1 )
	{
		printf("You must supply a string on the command line.\n");
		exit(1);
	}
	printf("%s\n", argv[argc-1]);
	strrev(argv[argc-1]);
	printf("%s\n", argv[argc-1]);
	return(0);
}
