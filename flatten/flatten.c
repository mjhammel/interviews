/*
 * Flatten a tree, that is a link-list where each node may
 * or may not have one child list.  Each child list is appended
 * to the end of the main list.
 *
 * We generate a main set of nodes and randomly choose which
 * ones will have child nodes.  
 * Main node values are in the range of 0-255.
 * Child node values are in the range of 0-255 + n*1000.
 * where n increases for each new set of child nodes.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct _node {
    struct _node    *next;
    struct _node    *child;
    int             value;
} NODE_T;

NODE_T  *head = NULL;

NODE_T *genList(int numNodes, int incr)
{
    int i;
    NODE_T  *node=NULL, *last=NULL, *firstNode=NULL;

    /* Generate a main list */
    for(i=0; i<numNodes; i++)
    {
        node = (NODE_T *)malloc(sizeof(NODE_T));
        node->next = NULL;
        node->child = NULL;

        if ( firstNode == NULL )
            firstNode = node;

        node->value = rand()%256 + incr;
        // printf("value: %d\n", node->value);

        if ( last != NULL )
            last->next = node;
        last = node;
    }
    return firstNode;
}

void genChildren(int childNodes)
{
    NODE_T  *node=head, *child=NULL;
    int     i;

    i=1;
    while(node != NULL)
    {
        if ( rand()%2 )
        {
			printf("Adding child nodes to node %d\n", i);
            node->child=genList(childNodes, i*1000);
        }
        node = node->next;
        i++;
    }
}

void append(NODE_T **tail, NODE_T *head)
{
    NODE_T *node = head;

    (*tail)->next = node;

    while (node->next != NULL)
        node = node->next;
    *tail = node;
}

void flatten(NODE_T *head)
{
    NODE_T *node, *tail;

    /* Find the tail. */
    node = head;
    while (node->next != NULL)
        node = node->next;
    tail = node;

    /* Find nodes with children and append to main list. */
    node = head;
    while (node != NULL)
    {
        if ( node->child != NULL )
        {
            append(&tail, node->child);
            node->child = NULL;
        }
        node = node->next;
    }
}

void printList(NODE_T *node)
{
    while(node != NULL)
    {
        printf("%4d ", node->value);
        node = node->next;
    }
    printf("\n");
}

void printAll()
{
    NODE_T *node = NULL;

    printList(head);

    node = head;
    while(node != NULL)
    {
        if ( node->child != NULL )
            printList(node->child);
        node = node->next;
    }
}

int main(int argc, char **argv)
{
    int numNodes   = 0;
    int childNodes = 0;

    /* Initialize random number generator */
    srand(time(NULL));

    /*
     * Arguments:
     * 1: Number of main nodes.
     * 2. Number of nodes in a child.
     * Note: No error checking done here!
     */
    numNodes = atoi(argv[1]);
    childNodes = atoi(argv[2]);

    /* Gen the main list */
    head = genList(numNodes, 0);

    /* Gen child lists */
    genChildren(childNodes);

    /* Print generated lists */
    printAll();

    /* Now, flatten the list. */
    flatten(head);

    /* And print all lists again (there should just be the one). */
    printAll();
}
