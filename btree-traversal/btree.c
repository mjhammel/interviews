/*
 * C program for different tree traversals
 * Source: http://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
 */

#include <stdio.h>
#include <stdlib.h>
 
/* 
 * A binary tree node has data, pointer to left child 
 * and a pointer to right child 
 */
struct node
{
     int data;
     struct node* left;
     struct node* right;
};

/* 
 * Helper function that allocates a new node with the 
 * given data and NULL left and right pointers.
 */
struct node* newNode(int data)
{
     struct node* node = (struct node*)
                                  malloc(sizeof(struct node));
     node->data = data;
     node->left = NULL;
     node->right = NULL;
 
     return(node);
}

/* Display an ASCII graph of the node layout */
void visualizeNodes()
{
    printf(
            "         1   \n"
            "        / \\ \n"
            "       2   3 \n"
            "      / \\   \n"
            "     4   5   \n");
}
 
/* 
 * Given a binary tree, print its nodes according to the 
 * "bottom-up" postorder traversal.
 */
void printPostorder(struct node* node)
{
     if (node == NULL)
        return;
 
     // first recurse on left subtree
     printPostorder(node->left);
 
     // then recurse on right subtree
     printPostorder(node->right);
 
     // now deal with the node
     printf("%d ", node->data);
}
 
/* Given a binary tree, print its nodes in inorder*/
void printInorder(struct node* node)
{
     if (node == NULL)
          return;
 
     /* first recur on left child */
     printInorder(node->left);
 
     /* then print the data of node */
     printf("%d ", node->data);  
 
     /* now recur on right child */
     printInorder(node->right);
}
 
/* Given a binary tree, print its nodes in preorder*/
void printPreorder(struct node* node)
{
     if (node == NULL)
          return;
 
     /* first print data of node */
     printf("%d ", node->data);  
 
     /* then recur on left sutree */
     printPreorder(node->left);  
 
     /* now recur on right subtree */
     printPreorder(node->right);
}    

/* Find the height of a btree */
int height(struct node* node)
{
    int lheight;
    int rheight;

    if (node==NULL)
        return 0;
    else
    {
        /* compute the height of each subtree */
        lheight = height(node->left);
        rheight = height(node->right);

        /* use the larger one */
        if (lheight > rheight)
            return(lheight+1);
        else return(rheight+1);
    }
}

/* Print nodes at through to specified level */
void printLevel(struct node* node, int level)
{
    if (node == NULL)
        return;
    if (level == 1)
        printf("%d ", node->data);
    else
    {
        printLevel(node->left, level-1);
        printLevel(node->right, level-1);
    }
}

/* Given a binary tree, print its nodes in level-order */
void printLevelorder(struct node* node)
{
    int i;
    int h = height(node);
    for(i=1; i<=h; i++)
        printLevel(node, i);
}

/* Driver program to test above functions*/
int main()
{
     struct node *root   = newNode(1);
     root->left          = newNode(2);
     root->right         = newNode(3);
     root->left->left    = newNode(4);
     root->left->right   = newNode(5); 
 
     visualizeNodes();

     printf("\nPreorder traversal of binary tree is \n");
     printPreorder(root);
 
     printf("\nInorder traversal of binary tree is \n");
     printInorder(root);  
 
     printf("\nPostorder traversal of binary tree is \n");
     printPostorder(root);

     printf("\nLevel order traversal of binary tree is \n");
     printLevelorder(root);
 
     printf("\n\nHit ENTER to exit...");
     getchar();
     return 0;
}
