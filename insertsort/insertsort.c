/*
 * Implement an Insert Sort.
 * https://www.geeksforgeeks.org/insertion-sort/
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_DATA    10

void initList(char *data)
{
    int i;
    for(i=0; i<MAX_DATA; i++)
    {
        data[i] = rand() % 100;
    }
}

void printList(char *data)
{
    int i;
    for(i=0; i<MAX_DATA; i++)
    {
        printf("%2d ", data[i]);
    }
    printf("\n");
}

void sortList(char *data)
{
    int i, j;
    for(i=1; i<MAX_DATA; i++)
    {
        for(j=i; j>0; j--)
        {
            if ( data[j] < data[j-1] )
            {
                /* Swap values */
                data[j]   = data[j] ^ data[j-1];
                data[j-1] = data[j] ^ data[j-1];
                data[j]   = data[j] ^ data[j-1];
            }
        }
    }
}

int main(int argc, char **argv)
{
    char    data[MAX_DATA];

    srand(time(NULL));
    initList(data);
    printList(data);
    sortList(data);
    printList(data);
}
