/*
 * Convert a binary tree (unordered) into a proper
 * Max Heap.
 * https://johnderinger.wordpress.com/2012/12/28/heapify/
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_DATA    10

/*
 * Fill array with random data.
 * We assume no parent-child relationships here.
 */
void initData( int *data )
{
    int i;
    for(i=0; i<MAX_DATA; i++)
        data[i] = rand() % 100;
}

void printData( int *data )
{
    int i;
    for(i=0; i<MAX_DATA; i++)
    {
        printf("%2d ", data[i]);
    }
    printf("\n");
}

/*
 * Move data.
 */
void siftDown(int *head, int start, int end)
{
    // These are all indices into the *head array.
    int     root;
    int     child;
    int     tmp;

    root = start;
    while ( (2*root+1) <= end )
    {
        // Pick child with highest value
        child = 2*root+1;
        if ( (child+1)<end && (head[child]<head[child+1]) )
            child += 1;

        // Swap with root, if necessary
        if ( head[root] < head[child] )
        {
            tmp = head[child];
            head[child] = head[root];
            head[root] = tmp;
            root = child;
        }
        else
        {
            return;
        }
    }
}

/*
 * Start in the middle of the array and move
 * to the start, sorting as we go.
 */
void heapify(int *head)
{
    int start;
    int end = MAX_DATA-1;

    start = (end-1)/2;
    while(start>0)
    {
        siftDown(head, start, end-1);
        --start;
    }
}

int main(int argc, char **argv)
{
    int data[10];
    initData(data);
    printData(data);
    heapify(data);
    printData(data);
}
