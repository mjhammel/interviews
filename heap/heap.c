/*
 * Heap implementation. 
 * Generate a heap of specified size and content.
 * The print the content in sorted order, low to high.
 * See http://www.sanfoundry.com/c-program-implement-heap/
 *
 * Insert is O(n^2)
 * DeleteMin is O(n lg n)
 */

#include<stdio.h>
#include<string.h>
#include<limits.h>

/* 
 * Declaring heap globally so that we do not need to pass it as an argument every time.
 * Heap implemented can be either Min or Max based on command line arg.
 */
int heap[1000000], heapSize;

/* Initialize Heap */
void Init(int min) 
{
    heapSize = 0;
    if ( min )
        heap[0] = -INT_MAX;
    else
        heap[0] = INT_MAX;
}

/* 
 * Insert an element into the heap
 * by adding it to the end and then adjusting 
 * the heap elements.
 */
void Insert(int element, int min) 
{
    int current;

    heapSize++;
    heap[heapSize] = element; /* Insert in the last place */

    /* Adjust its position */
    current = heapSize;
    if ( min )
    {
        while (heap[current / 2] > element) 
        {
            heap[current] = heap[current / 2];
            current /= 2;
        }
    }
    else
    {
        while (heap[current / 2] < element) 
        {
            heap[current] = heap[current / 2];
            current /= 2;
        }
    }
    heap[current] = element;
}

void printHeap() 
{
    int i;

    for(i=1; i<=heapSize; i++)
    {
        printf("%d ", heap[i]);
    }
    printf("\n");
}

/*
 * Extract the minimum element from the heap.
 * Doing this reduces the heap by 1.
 */
int DeleteMin() 
{
    /* 
     * heap[1] is the minimum element. So we remove heap[1]. Size of the heap is decreased.
     * Now heap[1] has to be filled. We put the last element in its place and see if it fits.
     * If it does not fit, take minimum element among both its children and replaces parent with it.
     * Again see if the last element fits in that place.
     */
    int minElement, lastElement, child, current;
    minElement = heap[1];
    lastElement = heap[heapSize--];

    /* 
     * current refers to the index at which we are now.
     */
    for (current = 1; current * 2 <= heapSize; current = child) 
    {
        /* 
         * child is the index of the element which is minimum among both the children
         * Indexes of children are i*2 and i*2 + 1
         */
        child = current * 2;

        /*
         * child!=heapSize beacuse heap[heapSize+1] 
         * does not exist, which means it has only one child
         */
        if (child != heapSize && heap[child + 1] < heap[child]) 
        {
            child++;
        }

        /* 
         * To check if the last element fits or not it suffices to check if the last element 
         * is less than the minimum element among both the children
         */
        if (lastElement > heap[child]) 
        {
            heap[current] = heap[child];
        } 
        else 
        {
            /* It fits there */
            break;
        }
    }
    heap[current] = lastElement;
    return minElement;
}

/*
 * Extract the maximum element from the heap.
 * Doing this reduces the heap by 1.
 */
int DeleteMax() 
{
    /* 
     * heap[1] is the maximum element. So we remove heap[1]. Size of the heap is decreased.
     * Now heap[1] has to be filled. We put the last element in its place and see if it fits.
     * If it does not fit, take maximum element among both its children and replaces parent with it.
     * Again see if the last element fits in that place.
     */
    int maxElement, lastElement, child, current;
    maxElement = heap[1];
    lastElement = heap[heapSize--];

    /* 
     * current refers to the index at which we are now.
     */
    for (current = 1; current * 2 <= heapSize; current = child) 
    {
        /* 
         * child is the index of the element which is minimum among both the children
         * Indexes of children are i*2 and i*2 + 1
         */
        child = current * 2;

        /*
         * child!=heapSize beacuse heap[heapSize+1] 
         * does not exist, which means it has only one child
         */
        if (child != heapSize && heap[child + 1] > heap[child]) 
        {
            child++;
        }

        /* 
         * To check if the last element fits or not it suffices to check if the last element 
         * is less than the maximum element among both the children
         */
        if (lastElement < heap[child]) 
        {
            heap[current] = heap[child];
        } 
        else 
        {
            /* It fits there */
            break;
        }
    }
    heap[current] = lastElement;
    return maxElement;
}
int main(int argc, char **argv)
{
    int     number_of_elements;
    int     iter, element;
    int     MIN=0;
    char    *type;

    if ( argc > 1 && (strcasecmp(argv[1], "min") == 0) ) 
    {
        MIN=1;
    }
    type = (MIN)?"Min":"Max";

    printf("Program to demonstrate %s Heap:\nEnter the number of elements: ", type);
    scanf("%d", &number_of_elements);

    Init(MIN);

    printf("Enter the elements: ");
    for (iter = 0; iter < number_of_elements; iter++) 
    {
        scanf("%d", &element);
        Insert(element, MIN);
    }

    printHeap();

    if ( MIN )
    {
        for (iter = 0; iter < number_of_elements; iter++) 
        {
            printf("%d ", DeleteMin());
        }
    }
    else
    {
        for (iter = 0; iter < number_of_elements; iter++) 
        {
            printf("%d ", DeleteMax());
        }
    }
    printf("\n");
    return 0;
}
