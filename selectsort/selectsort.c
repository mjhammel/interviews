/*
 * Implement a select sort.
 * https://www.geeksforgeeks.org/selection-sort/
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_DATA    10

void initList(int *data)
{
    int i;
    for(i=0; i<MAX_DATA; i++)
    {
        data[i] = rand() % 100;
    }
}

void printList(int *data)
{
    int i;
    for(i=0; i<MAX_DATA; i++)
    {
        printf("%2d ", data[i]);
    }
    printf("\n");
}

int findMin(int *data, int idx )
{
    int i;
    int tmp = idx;
    for(i=idx+1; i<MAX_DATA; i++)
    {
        if ( data[i] < data[tmp] )
            tmp = i;
    }
    return(tmp);
}

void sortList(int *data)
{
    int i, j;

    for (i=0; i<MAX_DATA-1; i++)
    {
        j = findMin(data, i);
        if ( i != j )
        {
            data[i] = data[i] ^ data[j];
            data[j] = data[i] ^ data[j];
            data[i] = data[i] ^ data[j];
        }
    }
}
    
int main(int argc, char **argv)
{
    int    data[MAX_DATA];

    srand(time(NULL));
    initList(data);
    printList(data);
    sortList(data);
    printList(data);
}
