/*
 * Simple btree creation using three different methods:
 * 1. use three pointer vars
 * 2. use one pointer var
 * 3. using insert
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _node {
	int data;
	struct _node *left;
	struct _node *right;
} NODE_T;

NODE_T *root = NULL;
int count = 0;

// Allocate a node and initialize it
NODE_T *
newNode(int data)
{
	NODE_T *node = (NODE_T *)malloc(sizeof(NODE_T));
	memset(node, 0, sizeof(NODE_T));
	node->data = data;
	return node;
}

// Traverse tree to find a parent node for a new node
NODE_T *
insert(NODE_T *node, int data)
{
	if ( node == NULL ) 
		return (newNode(data));
	else
	{
		if ( data <= node->data )
			node->left = insert(node->left, data);
		else
			node->right = insert(node->right, data);
	    return node;
	}
}

// clear the tree
void
clear(NODE_T *node)
{
	if ( (node->left==NULL) && (node->right==NULL) ) 
		free(node);
	else
	{
		if ( node->left != NULL )
		{
			clear(node->left);
			node->left == NULL;
		}
		if ( node->right != NULL )
		{
			clear(node->right);
			node->right == NULL;
		}
	}
}

// Reset from a test
void
reset()
{
	clear(root);
	root = NULL;
    count = 0;
}

// Determine the size of the tree
int
size(NODE_T *node)
{
	if ( (node->left==NULL) && (node->right==NULL) ) 
		count += 1;
	else
	{
		if ( node->left != NULL )
			size(node->left);
		if ( node->right != NULL )
			size(node->right);
		count += 1;
	}
}

// print the tree
void
printNode(NODE_T *node)
{
	if ( node == NULL )
	{
		printf("Tree is empty");
		return;
	}

	if ( (node->left==NULL) && (node->right==NULL) ) 
		printf("Data: %d\n", node->data);
	else
	{
		if ( node->left != NULL )
			printNode(node->left);
		printf("Data: %d\n", node->data);
		if ( node->right != NULL )
			printNode(node->right);
	}
}

// build123a: use three pointer vars
void
build123a()
{
	NODE_T *leftNode = NULL;
	NODE_T *rightNode = NULL;
	root = newNode(2);
	leftNode = newNode(1);
	rightNode = newNode(3);
	root->left = leftNode;
	root->right = rightNode;
	printf("build123a:\n");
	printNode(root);
}

// build123b: use one pointer var
void
build123b()
{
	root = newNode(2);
	root->left = newNode(1);
	root->right = newNode(3);
	printf("build123b:\n");
	printNode(root);
}

// build123c: using insert
void
build123c()
{
	root = insert(root, 2);
	root = insert(root, 1);
	root = insert(root, 3);
	printf("build123c:\n");
	printNode(root);
}

int
main(int argc, char **argv)
{
	build123a();
	size(root);
	printf("Size = %d\n", count);
    reset();

	build123b();
	size(root);
	printf("Size = %d\n", count);
    reset();

	build123c();
	size(root);
	printf("Size = %d\n", count);
    reset();
}
