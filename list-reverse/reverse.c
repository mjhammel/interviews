/*
 * Reverse a link list
 * Source: http://algorithms.tutorialhorizon.com/reverse-a-linked-list/
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct _slist_t {
    struct _slist_t     *next;
    int                 data;
} SLIST_T;

SLIST_T     *slist_head = NULL;

void populate_slist()
{
    int     i;
    SLIST_T *currNode = NULL;
    SLIST_T *prevNode = NULL;

    for(i=1; i<=5; i++)
    {
        currNode = (SLIST_T *)calloc(1, sizeof(SLIST_T));
        currNode->data = i;
        currNode->next = NULL;
        if (prevNode)
            prevNode->next = currNode;
        if (!slist_head)
            slist_head = currNode;
        prevNode = currNode;
    }
}

void reverse()
{
    SLIST_T *currNode = slist_head;
    SLIST_T *prevNode = NULL;
    SLIST_T *nextNode = NULL;

    while(currNode!=NULL)
    {
        nextNode = currNode->next;
        currNode->next = prevNode;
        prevNode = currNode;
        currNode = nextNode;
    }
    slist_head = prevNode;
}

void print_list()
{
    SLIST_T *currNode = slist_head;

    while(currNode!=NULL)
    {
        printf("%2d ", currNode->data);
        currNode = currNode->next;
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    populate_slist();
    print_list();
    reverse();
    print_list();
}
