/*
 * Singly-linked list management.
 * Creates, inserts and delets links.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct __mylink_t {
    int value;
    struct __mylink_t *next;
} MYLINK_T;

/* Pointer to head of link list */
MYLINK_T *head = NULL;
int idx = 0;

/* Allocate link list */
void
allocate(int count)
{
    int i;
    MYLINK_T *ptr;

    for(i=0; i<count; i++)
    {
        if ( head == NULL )
        {
            head=(MYLINK_T *)calloc(1, sizeof(MYLINK_T));
            ptr=head;
        }
        else
        {
            ptr->next=(MYLINK_T *)calloc(1, sizeof(MYLINK_T));
            ptr=ptr->next;
        }
    }
}

/* Allocate link list */
void
fill()
{
    int i=0;
    MYLINK_T *ptr = head;
    while ( ptr != NULL )
    {
        ptr->value = idx++;
        ptr = ptr->next;
    }
}

/* Travere (and print) link list */
void
traverse()
{
    MYLINK_T *ptr = head;
    printf("Values: ");
    while ( ptr != NULL )
    {
        printf("%d ", ptr->value);
        ptr = ptr->next;
    }
    printf("\n");
}

/* 
 * Insert a link
 * "pos" is 0-based
 */
void
insert(int pos)
{
    int i=0;
    MYLINK_T *ptr = head;
    MYLINK_T *newlink = NULL;
    for ( i=0; i<pos; i++ ) 
    {
        if ( ptr != NULL )
            ptr = ptr->next;
        else
        {
            printf("ptr == NULL @ position %d\n", pos);
            return;
        }
    }
    if ( ptr == NULL )
    {
        printf("Invalid position request: %d\n", pos);
        return;
    }
    printf("inserting at %d\n", pos);
    newlink=(MYLINK_T *)calloc(1, sizeof(MYLINK_T));
    newlink->value = idx++;
    newlink->next=ptr->next;
    ptr->next=newlink;
}

/* 
 * Delete a link
 * "pos" is 0-based
 */
void
delete(int pos)
{
    int i=0;
    MYLINK_T *ptr = head;
    MYLINK_T *lastlink = NULL;

    /* If we're deleting the head. */
    if (pos == 0)
    {
        ptr = head;
        head = head->next;
        free(ptr);
        return;
    }

    /* Run the list till the next one is the one to delete. */
    for ( i=0; i<pos-1; i++ ) 
    {
        if ( ptr != NULL )
            ptr = ptr->next;
        else
        {
            printf("ptr == NULL @ position %d\n", pos);
            return;
        }
    }
    if ( ptr == NULL )
    {
        printf("Invalid position request: %d\n", pos);
        return;
    }

    if ( ptr->next != NULL)
    {
        printf("deleting at %d\n", pos);
        lastlink=ptr->next;
        ptr->next=(ptr->next)->next;
        free(lastlink);
    }
    else
        printf("Invalid position request: %d\n", pos);
}

int
main(int argc, char **argv)
{
    int count = 0;
    int ipos = -1;
    int dpos = -1;

    if (argc != 4)
    {
        printf("%s <links> <insert> <delete>\n", argv[0]);
        exit(1);
    }
    count = atoi(argv[1]);
    if (count < 0)
    {
        printf("Invalid count %d - try agin.\n", count);
        exit(1);
    }
    ipos = atoi(argv[2]);
    if (ipos < 0)
    {
        printf("Invalid insert position %d - try agin.\n", ipos);
        exit(1);
    }
    dpos = atoi(argv[3]);
    if (dpos < 0)
    {
        printf("Invalid delete position %d - try agin.\n", dpos);
        exit(1);
    }

    /* Allocate Link List */
    allocate(count);

    /* Fill it. */
    fill();

    /* Traverse it */
    traverse();

    /* Insert a link */
    insert(ipos);
    traverse();

    /* Delete a Link */
    delete(dpos);
    traverse();

    /* B'Bye */
}
