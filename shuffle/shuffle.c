/*
 * Shuffle a deck of cards.
 * See: http://www.geeksforgeeks.org/shuffle-a-given-array/
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_CARDS   52

int     cards[NUM_CARDS];
char    *cardNames[NUM_CARDS] = {
        "2H", "3H", "4H", "5H", "6H", "7H", "8H", "9H", "10H", "JH", "QH", "KH", "AH",
        "2K", "3K", "4K", "5K", "6K", "7K", "8K", "9K", "10K", "JK", "QK", "KK", "AK",
        "2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "10C", "JC", "QC", "KC", "AC",
        "2D", "3D", "4D", "5D", "6D", "7D", "8D", "9D", "10D", "JD", "QD", "KD", "AD"
};

void initDeck()
{
    int i;
    for(i=0; i<NUM_CARDS; i++)
        cards[i]=i;
}

void swap(int i, int j)
{
    cards[i] = cards[i] ^ cards[j];
    cards[j] = cards[i] ^ cards[j];
    cards[i] = cards[i] ^ cards[j];
}

void shuffleDeck()
{
    int i;
    int j;

    for(i=NUM_CARDS-1; i>0; i--)
    {
        j = rand() % (i+1);
        swap(i, j);
    }
}

int getCard( int idx )
{
    return cards[idx];
}

void printCards()
{
    int i,j;

    for(i=0; i<52; i++)
    {
        printf("%3s ", cardNames[cards[i]]);
        if ( (i>0) && ((i+1)%13) == 0 )
            printf("\n");
    }
    printf("\n");
}

int main (int argc, char **argv)
{
    srand(time(NULL));
    initDeck();
    printCards();
    shuffleDeck();
    printf("Random card: %s\n", cardNames[getCard( rand() % NUM_CARDS )] );
    printCards();
}
