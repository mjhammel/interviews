/*
 * Example of the use of unions in a data structure.
 */

#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) 
{
	typedef union {
		int a;
		char b[10];
		float c;
	} Union;

    /*
     * Initialize int value in both variables.
     */
	Union x,y = {100};

    /* Reset a in X */
	x.a = 50;

    /* Reset b in X */
	strcpy(x.b,"hello");

    /* Reset b in X */
	x.c = 21.50;

    /*
     * Result:
     * x.c overrides all others so nothing in x.b
     * 100 == 'd' so y.b actually prints something.
     */
	printf("Union x : %d %s %f \n",x.a,x.b,x.c);
	printf("Union y : %d %s %f \n",y.a,y.b,y.c);
}
