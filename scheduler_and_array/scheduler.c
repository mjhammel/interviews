/*
 * Schedule some functions to be run at given time offsets.
 * There are three functions.  The first runs immediately, the next
 * 5 seconds later and the last 10 seconds after the first.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>
#include <math.h>
#include <string.h>

/*
 * Task structure that includes the "what" and "when".
 * Stored in a singly linked list.
 */
typedef struct _task_t {
    void (*func)(uint64_t); // function pointer
    uint64_t runtime;       // the time at which we want to run this task
    struct _task_t *next;   // next item in list
} task_t;

/* The task list is initially empty. */
task_t * head = NULL;

/* System time, in millis */
uint64_t sys_ms;

/* Convert seconds and nanosecs to milliseconds */
void get_current_time_with_ms (void)
{
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);
    sys_ms = (uint64_t) ((spec.tv_sec * 1000) + round(spec.tv_nsec * 1.0e-6));
}

/* This is the task that's getting called at each scheduled time. */
void cb(uint64_t sched) 
{
    time_t now;
    char buf[128];

    now = time(NULL);
    sprintf(buf, "%s", ctime(&now));
    memset(buf+strlen(buf)-1, 0, 1);
    fprintf(stderr, "CB called (%s): %x, sys_ms = %x\n", buf, sched, sys_ms);
    fflush(stderr);
}

/* 
 * This is the scheduler.  It allocates and fills a task structure then appends it
 * to the task list.  The new task is inserted in order of when the task should run.
 */
void ScheduleTask(void* callback, int64_t delay_ms) 
{
    task_t *new_task = NULL;
    uint64_t target_time = sys_ms + delay_ms;
    task_t *cursor = head;

    /* This NULLs the next pointer for the new entry */
    new_task = (task_t *)calloc(sizeof(task_t),1);
    new_task->func = callback;
    new_task->runtime = target_time;
    
    /* are we inserting into an empty queue? */
    if (head == NULL) 
    {
        head = new_task;
    }
    else 
    {
        /* find the right spot for this callback */
        while((cursor->next) && (cursor->runtime < target_time)) 
        {
            cursor = cursor->next;
        }

        /* insert newly created task */
        new_task->next = cursor->next;
        cursor->next = new_task;
    }
}

int main( ) 
{

    time_t now;
    head = NULL;
    task_t *task = head;
    task_t *cursor = head;
    task_t *prev = NULL;

    /* Initialize system time */
    get_current_time_with_ms ();
    now = time(NULL);
    printf("sys_ms init: %x\n", sys_ms);

    /* Schedule some tasks.  Don't do this in order - let the scheduler handle that. */
    printf("Now: %s", ctime(&now));
    ScheduleTask(cb, 1000);
    printf("Scheduled task for now + 1 second.\n");
    ScheduleTask(cb, 10000);
    printf("Scheduled task for now + 10 seconds.\n");
    ScheduleTask(cb, 5000);
    printf("Scheduled task for now + 5 seconds.\n");

    while(head) 
    {
        /* Update system time */
        get_current_time_with_ms ();

        /* workqueue is not empty */
        cursor = head;
        prev = NULL;
            
        /* if it is time to run or we're overdue */
        while(cursor) 
        {
            task = NULL;
            if ( cursor->runtime <= sys_ms ) 
            {
                /* execute */
                cursor->func(cursor->runtime);

                /* Remove the link */
                if ( prev == NULL )
                    head = cursor->next;
                else
                    prev->next = cursor->next;
                task = cursor;
            }
            else
                prev = cursor;

            /* advance */
            cursor = cursor->next;

            /* free the memory associated with the task */
            if (task)
                free(task);
        }
    } 

    return 0;
}
