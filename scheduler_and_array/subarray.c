#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>

/*
 * ---------------------------------------------
 * Find the sub-array with the largets sum.
 * The array will be filled with random positive
 * and negative integers.
 * ---------------------------------------------
 */

/* 
 * This will point to our array of values.
 * It is dynamically allocated so we can test with different
 * array sizes.
 */
int *data = NULL;

/* Globals, to make it easier to share between methods. */
int sum, max_sum;
int start, end;

/*
 * ---------------------------------------------
 * Functions
 * ---------------------------------------------
 */

/*
 * Generate a set of random numbers in an array, some with negative values.
 * Clamp the values to 15.
 */
void init( int number_of_elements )
{
    int seed, fd, i;

    /* Open /dev/urandom and read 4 bytes for our seed. */
    fd = open("/dev/urandom", O_RDONLY);
    read(fd, &seed, 4);
    close(fd);

    /* Seed the RNG */
    srand(seed);

    data = (int *)calloc( number_of_elements, sizeof(int) );
    for (i=0; i<number_of_elements; i++)
    {
        data[i] = random() % 16;
        if ( random() % 2 )
            data[i] *= -1;
    }
}

/*
 * Display the array of elements.
 */
void printArray( int number_of_elements )
{
    int i;

    for (i=0; i<number_of_elements; i++)
        printf("%3d ", data[i]);
    printf("\n");
}

/*
 * Naive method: iteratre two loops through the data.
 */
void naive( int number_of_elements )
{
    int i, j, k;

    end = 0;
    start = 0;
    max_sum = data[0];
    for (i=0; i<number_of_elements; i++)
    {
        for (j=i; j<number_of_elements; j++)
        {
            if ( (i==0) && (j==number_of_elements-1) )
                continue;
            sum = 0;
            for(k=i; k<=j; k++)
            {
                sum += data[k];
            }
            // printf("i, j, k, sum, max: %d, %d, %d, %3d, %3d\n", i, j, k, sum, max_sum);
            if (sum>max_sum)
            {
                start = i;
                max_sum = sum;
                end = j;
            }
        }
    }
}

/* A couple of utility functions to find max values */
int max2( int a, int b )        {return (a>b)?a:b;}
int max3( int a, int b, int c ) {return max2(max2(a,b),c);}

/* 
 * Computes the sum across the mid point.
 */
int maxCrossingSum(int low, int mid, int high) 
{ 
    int i, sum, right_sum, left_sum;

    /* Include elements on left of mid. */
    sum = 0; 
    left_sum = INT_MIN; 
    for (i = mid; i >= low; i--) 
    { 
        sum = sum + data[i]; 
        if (sum > left_sum) 
          left_sum = sum; 
    } 
  
    /* Include elements on right of mid */
    sum = 0; 
    right_sum = INT_MIN; 
    for (i = mid+1; i <= high; i++) 
    { 
        sum = sum + data[i]; 
        if (sum > right_sum) 
          right_sum = sum; 
    } 
  
    /* Return sum of elements on left and right of mid */
    return left_sum + right_sum; 
}

/*
 * Divides the array and finds max sums for each subdivision.
 */
int maxSubArraySum(int low, int high)
{ 
    int left, mid, right, crossing;

    /* Base Case: Only one element */
    if (low == high) 
        return data[low]; 
  
    /* Find middle point */
    mid = (low + high)/2; 
  
    /* 
     * Return maximum of following three possible cases 
     * a) Maximum subarray sum in left half 
     * b) Maximum subarray sum in right half 
     * c) Maximum subarray sum such that the subarray crosses the midpoint
     */
    left     = maxSubArraySum(low, mid);
    right    = maxSubArraySum(mid+1, high);
    crossing = maxCrossingSum(low, mid, high);

   return max3(left, right, crossing);
} 

int divideAndConquer( int number_of_elements )
{
    max_sum = maxSubArraySum( 0, number_of_elements-1 );
}

/* 
 * ---------------------------------------------
 * main
 * ---------------------------------------------
 */

int main(int argc, char **argv)
{
    int number_of_elements, iter;

    printf("Program to demonstrate max sub-array sum:\nEnter the number of elements: ");
    scanf("%d", &number_of_elements);

    /* Generate a set of random numbers in an array. */
    init(number_of_elements);
    printArray(number_of_elements);

    /* 
     * Naive method is O(n^2), but has lower overhead in function calls because
     * it is not recursive. It's also KISS (simple).
     */
    naive( number_of_elements );
    printf ("Max sum: %d, start: %d, end: %d\n", max_sum, start, end);

    /*
     * Divide and Conquer method splits the data multiple times, recursively.
     * This is O(nlogn) but has higher function call overhead.
     * It also doesn't make finding the subarray indices easy.  We skip them here.
     * In general, this would be considered faster but harder to debug and maintain
     * if we need to know the subarray indices.
     */
    divideAndConquer( number_of_elements );
    printf ("Max sum: %d\n", max_sum);
}
