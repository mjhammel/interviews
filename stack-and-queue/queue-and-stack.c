/*
 * Fill an array with values.
 * Print the array as a Stack (FIFO).
 * Print the array as a Queue (LIFO).
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int main (int argc, char **argv)
{
    int     i, size=0;
    int     opt;
    int     *obj;
    int     *ptr;

    /* Initialize random number generator */
    srand(time(NULL));

    /* Get size of object */
    while ( (opt=getopt(argc, argv, ":s:")) != -1 )
    {
        switch (opt) {
            case 's': 
                size=atoi(optarg);
                break;
        }
    }
    if ( size == 0 )
    {
        printf("You must provide a size (-s)\n");
        exit(1);
    }

    /* Allocate the object */
    obj = (int *)calloc(size, 4);
    ptr = obj;

    /* Fill it in with random data. */
    for( i=0; i<size; i++ )
        *ptr++ = rand() % 100;

    printf("Initial Object as allocated:\n");
    ptr = obj;
    for( i=0; i<size; i++ )
        printf("%3d ", *ptr++);
    printf("\n");

    /* Queue is a FIFO, so it looks like the original */
    printf("Queue Object is a FIFO:\n");
    ptr = obj;
    for( i=0; i<size; i++ )
        printf("%3d ", *ptr++);
    printf("\n");

    /* Stack is a LIFO, so it looks opposite of the original */
    printf("Stack Object is a LIFO:\n");
    ptr--;
    for( i=0; i<size; i++ )
        printf("%3d ", *ptr--);
    printf("\n");

    /* Cleanup */
    free(obj);

}
