/*
 * Complete binary tree in breadth first order
 * implementated as an array.
 *
 * See: https://en.wikipedia.org/wiki/Binary_trees#Arrays
 * Algorithm: If node == i then left == 2i+1 and right = 2i+2
 */

#include <stdio.h>
#include <string.h>

int tree[10];

void
printTree()
{
	int 	n=0;

	for (n=0; n<10; n++)
		printf("Node %d: %d\n", n, tree[n]);
	printf("\n");
}

void
fillTree()
{
	int i;
	int m=10;

	memset(tree, 0, m);
	printTree();

	for(i=0; i<5; i++)
	{
		if ( tree[i] == 0 )
			tree[i] = m--;
		tree[2*i+1] = m--;
		tree[2*i+2] = m--;
	}

}

int
main(int argc, char **argv)
{
	/* Populate the tree with some data */
	fillTree();

	/* Now visit each node. */
	printTree();
}
